<?php

namespace App;

/**
 * Adherent object
 */
class Adherent
{
    public $nom;
    public $adresse;
    public $postcode;
    public $city;
    public $type;
    public $latitude;
    public $longitude;
    public $dateFirstMembership;

    /**
     * Adherent constructor
     *
     * @param $nom                 adherent's name
     * @param $adresse             adherent's address
     * @param $postcode            adherent's postcode
     * @param $city                adherent's city
     * @param $type                adherent's type
     * @param $latitude            adherent's latitude (optional)
     * @param $longitude           adherent's longitude (optional)
     * @param $dateFirstMembership adherent's first membership date (optional)
     */
    public function __construct($nom, $adresse, $postcode, $city, $type, $latitude = null, $longitude = null, $dateFirstMembership = null)
    {
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->type = $type;

        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->dateFirstMembership = $dateFirstMembership;
    }

    /**
     * Convert to GeoJSON structure
     *
     * GeoJSON is a format for encoding a variety of geographic data structures.
     * This method converts an Adherent objet to GeoJSON structure
     *
     * Warning: it's not JSON, but a PHP array!
     * use json_encode() to convert to JSON
     *
     * @see https://geojson.org/
     * @see https://tools.ietf.org/html/rfc7946 GeoJSON specification
     *
     * @return array
     */
    public function toGeoJSON(): array
    {
        if (is_null($this->latitude) || is_null($this->longitude)) {
            throw new \Exception("Latitude or longitude is null");
        }

        $geojson = [
            "type" => "Feature",
            "geometry" => [
                "type" => "Point",
                "coordinates" => [$this->longitude, $this->latitude]
            ],
            "properties" => [
                "name" => $this->nom,
                "type" => $this->type,
                "dateFirstMembership" => $this->dateFirstMembership,
            ]
        ];
        return $geojson;
    }
}
