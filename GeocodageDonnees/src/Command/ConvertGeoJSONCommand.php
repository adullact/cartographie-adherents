<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Adherent;

/**
 * Class ConvertGeoJSONCommand
 *
 * @package App\Command
 */
final class ConvertGeoJSONCommand extends Command
{
    /**
     * In this method setup command, description, and its parameters
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('convertgeojson');
        $this->setDescription('Convertit un fichier CSV type sortie API Adresse vers un fichier GeoJSON');
        $this->addArgument('inputfilename', InputArgument::REQUIRED, "Fichier d'entrée, format géocodé API Adresse");
        $this->addArgument('outputfilename', InputArgument::REQUIRED, "Fichier de sortie, format GeoJSON");
    }

    /**
     * Convert API Adresse CSV output into GeoJSON file
     *
     * @param InputInterface  $input  Symfony console input
     * @param OutputInterface $output Symfony console output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputfilename = $input->getArgument('inputfilename');
        $outputfilename = $input->getArgument("outputfilename");
        $logger = new ConsoleLogger($output);
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $logger->notice("Converting " . $inputfilename . " to GeoJSON…");
        $rows = $serializer->decode(file_get_contents($inputfilename), "csv");

        $geoJSON = [];
        foreach ($rows as $row) {
            $type = $row["input_type"] ?? $row["result_type"];

            if (empty($row["result_housenumber"])) {
                $adresse = $row["result_name"];
            } else {
                $adresse = $row["result_housenumber"] . ", " . $row["result_name"];
            }

            $city = $row["result_city"];
            $postcode = $row["result_postcode"];

            $adh = new Adherent($row["nom"], $adresse, $postcode, $city, $type, $row["latitude"], $row["longitude"], $row["date_first_membership"]);

            if (empty($adh->latitude) || empty($adh->longitude)) {
                $logger->warning("Ignored " . $adh->nom . ". Address not found: latitude or longitude is empty");
            } else {
                array_push($geoJSON, $adh->toGeoJSON());
                $logger->debug('Added line to GeoJSON file: ' . $adh->nom . " (" . $adh->latitude . "/" . $adh->longitude . ")");
            }
        }
        file_put_contents($outputfilename, json_encode($geoJSON));
        $logger->notice("Converting done.");

        return self::SUCCESS;
    }
}
