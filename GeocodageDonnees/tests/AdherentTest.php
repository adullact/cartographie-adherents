<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Adherent;

/**
 * Tests related to Adherent
 *
 * @link App\Adherent
 */
final class AdherentTest extends TestCase
{
    /**
     * Assert we can create an Adherent
     *
     * @return void
     */
    public function testCanCreateAdherent(): void
    {
        $this->assertInstanceOf(
            Adherent::class,
            $adh = new Adherent("Ville de Montpellier", "1, place Georges Frêche", "34000", "Montpellier", "ville")
        );
    }

    /**
     * Assert we can't convert an adherent to GeoJSON if longitude or latitude is not provided
     *
     * @return void
     */
    public function testCannotConvertToGeoJSONWithoutLonLat(): void
    {
        $this->expectException(\Exception::class);

        $adh = new Adherent("Ville de Montpellier", "1, place Georges Frêche", "34000", "Montpellier", "ville");
        $adh->toGeoJSON();
    }

    /**
     * Assert we can convert an adherent to GeoJSON if longitude and latitude are provided
     *
     * @return void
     */
    public function testConvertGeoJSON(): void
    {
        $adh = new Adherent("Ville de Montpellier", "1, place Georges Frêche", "34000", "Montpellier", "ville", "48.846521", "2.369725", "01/01/2000");
        $geojson = $adh->toGeoJSON();
        $coordinates = $geojson["geometry"]["coordinates"];
        $this->assertSame("48.846521", $adh->latitude);
        $this->assertSame("2.369725", $adh->longitude);
        $this->assertSame("2.369725", $coordinates[0]);
        $this->assertSame("48.846521", $coordinates[1]);
        $this->assertSame("01/01/2000", $adh->dateFirstMembership);
        $this->assertSame("01/01/2000", $geojson["properties"]["dateFirstMembership"]);
    }
}
