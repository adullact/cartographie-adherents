# Déploiement

## Prérequis

```bash
# téléchargement du dépôt
git clone https://gitlab.adullact.net/adullact/cartographie-adherents
cd cartographie-adherents

# php 8
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt install php8.0

# Installation de Composer 2.0.14
# (Adapté depuis .gitlab/ci/Dockerfiles/php_composer_phive/Dockerfile)
export COMPOSER_VERSION="2.0.14"
cd /tmp && \
    curl -sSL https://getcomposer.org/installer --output composer-setup.php && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --version="$COMPOSER_VERSION" && \
    rm -v composer-setup.php && \
    chmod +x composer.phar
sudo mv -v composer.phar /usr/local/bin/composer

# installation des dépendances
sudo apt install php-xml
cd GeocodageDonnees/
composer install
```

## Géocodage des donnéees

Voir la [définition du géocodage](geocodage.md).

Un exemple avec une dizaine d'adhérents est déjà géocodé.
Si vous voulez voir l'exemple par défaut, vous pouvez passer à
l'étape `Visualisation des données`.
Sinon, vous pouvez utiliser et géocoder vos propres données :

Les formats sont expliqués dans [la documentation geocodage](geocodage.md).

```bash
# Étape non obligatoire si la source des données est déjà au format API Adresse.
# Voir comment exporter le tableur des adhérents au format CSV dans Documentation/export_csv_tableur_adherents.md

# Conversion tableur des adhérents -> format API Adresse (si nécessaire)
# `inputfilname` : fichier au format tableur des adhérents (fichier CSV)
# `outputfilename` : fichier au format API Adresse (entrée)
./GeocodageDonnees/bin/app_console adherentsspreadsheet -v <inputfilename> <outputfilename>

# Géocodage des données au format API Adresse.

# * `inputfilename` : fichier au format API Adresse (en entrée, comme défini dans Documentation/geocodage.md).
#   Correspond au fichier outputfilename de la commande précédente.
# * `outputfilename` : fichier format API Adresse (en sortie)
./GeocodageDonnees/bin/app_console geocode -v <inputfilename> <outputfilename>

# Conversion des données : format API Adresse sortie -> format GeoJSON
# inputfilename : fichier format API Adresse (sortie). Correspond au fichier outputfilename de la commande précédente.
# outputfilename : fichier format GeoJSON
./GeocodageDonnees/bin/app_console convertgeojson -v <inputfilename> <outputfilename>
```

Une fois produit, c'est le fichier GeoJSON qui est lu par `VisualisationDonnees/src/cartographie.js`
lors de l'appel à la fonction `fetchJSON(url)`.
Il convient soit de modifier la variable `url`, soit de modifier le
contenu de `Exemples/APartirFormatTableurAdherents/output.geojson`.

Les options `-v` et `-vv` permettent d'afficher plus d'informations concernant le traitement des données.
Plus d'explications dans la [documentation de journalisation](journalisation.md).

## Visualisation des données

La visualisation des données est sous la forme de cartographie.
Par défaut, un exemple d'une dizaine d'adhérent est utilisé.

Il suffit de lancer un serveur HTTP _à la racine du dépôt_, puis ouvrir le
dossier `VisualisationDonnes/src/` dans le navigateur.

Remarque : Un serveur HTTP est nécessaire puisque les navigateurs protègent
l'accès aux fichiers locaux ; ouvrir le fichier HTML uniquement provoquerait
des erreurs (CORS).

```bash
# Lancez un serveur HTTP (au choix)
python3 -m http.server      # avec python 3
python2 -m SimpleHTTPServer # avec python 2
php -S localhost:8000       # avec php-cli

# Dans un navigateur, consultez l'URL suivante :
# http://localhost:8000/VisualisationDonnees/src/
```
