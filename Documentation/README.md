# Architecture technique

Ce dépôt est séparé en deux parties :

- géocodage des données dans `GeocodageDonnees/`
- visualisation des données `VisualisationDonnees/`

Des exemples de jeux de données peuvent se trouver dans le dossier `Exemples/`.

Le dossier `Donnees/` est un dossier dont le contenu est ignoré par git.  
Il peut servir à placer les fichiers de données : fichier utilisé en entrée par le géocodage des données, en sortier (GeoJSON)…
