# CHANGELOG

Tous les changements notables seront documentés dans ce fichier.

Ce fichier s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) et [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Pas encore publié - 202X-XX-XX

- …

## 1.2.0 - 2021-06-16

### Nouveau

- Afficher le nombre d'adhérents pour chaque type.

### Corrigé

- La date d'adhésion n'est plus affichée, seule l'année l'est.

## 1.1.0 - 2021-06-08

### Nouveau

- Filtrer par type d'adhérent.
- Ajout de la date de première adhésion d'un adhérent.
- Informer et avertir les utilisateurs sur le fonctionnement du géocodage avec le logger.
    Ex: L'utilisateur est avertit si une adresse n'est pas renseignée ou trouvée.
- Ajout d'une CI.
- Documentation de l'export du tableur des adhérents au format CSV.

### Corrigé

- Les adhérents dont l'adresse n'est pas trouvée ou renseignée ne sont plus affichés.

## 1.0.0 - 2021-05-19

### Nouveau

- Affichage du type d'adhérent
- Automatisation du géocodage
