let mapLL;

const defaultTileServerURL = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const defaultAttribution = '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';

let zoom = 2;
let coordsLatLon = [20, 20];
loadMap(coordsLatLon, zoom);

let url = "../../Exemples/APartirFormatTableurAdherents/output.geojson"; // Données de démo
// let url = "../../Donnees/adherents2021.geojson"; // Données réelles (une fois le fichier GeoJSON construit)
let jsonPromise = fetchJSON(url);
loadGeoJSON(jsonPromise);

addFilterElement();

function loadMap(coordsLatLon = [0, 0], zoom = 2, tileServerURL = defaultTileServerURL, attribution = defaultAttribution, maxzoom = 19) {
    mapLL = L.map(document.querySelector("#mapid"));
    L.tileLayer(tileServerURL, {
        maxZoom: maxzoom,
        attribution: attribution
    }).addTo(mapLL);
    mapLL.setView(coordsLatLon, zoom);
}

let geoJSON = null;

function fetchJSON(url) {
    return fetch(url)
        .then(function (response) {
            return response.json();
        });
}

function loadGeoJSON(jsonPromise, type = "0_tous") {
    jsonPromise.then(function (json) {
        geoJSON = L.geoJSON(json, {
            filter: function (feature, layer) {
                // Filter by type
                return type === undefined || type === "0_tous" || type === "1_separator" ? true : feature.properties.type === type;
            }
        });
        geoJSON.bindPopup(function (layer) {
            let properties = layer.feature.properties;
            let type = AdherentType.translateTypeName(properties.type);
            let description = `<strong>${properties.name}</strong><br>Type : ${type}`;

            // this field is optional
            if (properties.dateFirstMembership) {
                description = `${description}<br>Année de première adhésion : ${properties.dateFirstMembership}`;
            }
            return description;
        }).addTo(mapLL);
    });

}

function removeMarkers() {
    // Close the popup first.
    // It guarantees that the popup won't stay if the marker is removed
    mapLL.closePopup();

    // Remove all markers
    geoJSON.eachLayer(function (layer) {
        mapLL.removeLayer(layer);
    });
}

function addFilterElement() {
    let filterElement = document.createElement("select");

    // Should not be harcoded but moved to GeoJSON generation
    // see https://gitlab.adullact.net/adullact/cartographie-adherents/-/issues/47
    const types = [new AdherentType("0_tous"), new AdherentType("1_separator"), new AdherentType("CDG"),
    new AdherentType("Etablissement public"), new AdherentType("SDIS"),
    new AdherentType("Ville"), new AdherentType("Région"),
    new AdherentType("Hôpital"), new AdherentType("Groupement public (GIE, GIP, SIH, …)"),
    new AdherentType("EPCI"), new AdherentType("Entreprise"), new AdherentType("Education"),
    new AdherentType("Département"), new AdherentType("Association coll."),
    new AdherentType("Association"), new AdherentType("Administration centrale")
    ];

    // order types alphabetically (for accessibility reasons)
    const typesAlphabeticallyOrdered = types.sort(function (a, b) {
        return a.name.localeCompare(b.name);
    });

    jsonPromise.then(function (json) {

        // Count number of type occurencies
        // It could be improved
        // see https://gitlab.adullact.net/adullact/cartographie-adherents/-/issues/48
        for (let obj of json) {
            let adherentType = obj.properties.type;

            for (let type of types) {
                if (type.name == adherentType) {
                    type.occurencies++;
                } else if (type.name == "0_tous" && type.occurencies == 0) {
                    type.occurencies = json.length;
                }
            }
        }

        // Create an option for each type
        for (let type of typesAlphabeticallyOrdered) {
            let option = document.createElement("option");
            option.value = type.name;
            let innerHTML = `${type.translatedName}`;
            if (type.name !== "1_separator") {
                innerHTML = `${innerHTML} - ${type.occurencies}`;
            }
            option.innerHTML = innerHTML;

            // Select default option
            if (type.name == "0_tous") {
                option.selected = true;
            }
            filterElement.appendChild(option);
        }
    });

    document.querySelector("body").appendChild(filterElement);

    filterElement.addEventListener("change", function () {
        removeMarkers();
        loadGeoJSON(jsonPromise, this.value);
    });
}
