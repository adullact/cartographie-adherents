# Exemples

Pour générer la cartographie des adhérents, les données ne sont pas fournies dans ce dépôt.  
Cependant, il y a deux exemples de données. Voici la différence :

- `APartirFormatApiAdresse/` :  
Les données sont stockées dans le fichier `input.csv` qui utilise le format de l'API Adresse.

- `APartirFormatTableurAdherents/` :  
Les données sont stockées dans le fichier `input.csv` qui utilise le format du tableur des adhérents utilisé par l'ADULLACT.

Le fichier `output.geojson` est lisible par la cartographie.
